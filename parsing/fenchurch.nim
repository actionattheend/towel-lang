import os, sugar, sequtils, strutils

const RAW_CATS = readFile("lexing.categories.config").strip.split("\n\n")
const SYM_KINDS = ["nl_cr","sep","enclose","oper_sym"]

proc process_category(cat: string): (string, seq[string]) =
  let catlines = cat.split('\n')
  let title = catlines[0][1..^2]
  result = (title, catlines[1..^1])

const UNRESERVED = readfile("lexing.unreserved.config").strip.split("\n\n")
var NAME_FIRST_CHARS, NAME_ADD_CHARS: string
for un_entry in UNRESERVED:
  var cat: string
  var glist = newSeq[string]()
  (cat, glist) = process_category(un_entry)
  if cat == "name_first_chars":
    NAME_FIRST_CHARS = glist[0]
  elif cat == "name_additional_chars":
    NAME_ADD_CHARS = glist[0]
  else:
    # space reserved for additional namelike tokens
    discard
var NUMBER_FIRST_CHARS = "0123456789"
var NUMBER_ADD_CHARS = ".e"

proc pad(s: string): string =
  result = " " & s & " "

proc parse_cat_entry(kind: string, entry: string): (string, string) =
  var lex, map: string
  if "=>" in entry:
    (lex, map) = entry.strip.split("=>")
  else:
    lex = entry
    map = entry.replace(" ","")
  lex = lex.strip
  if lex.count('"') > 1:
    lex = lex.replace("\"","")
  map = map.strip.replace("\"","")
  map = "@@" & kind & "@" & map
  result = (lex,map)

proc tag_unreserved(chunk: string): string =
  if chunk.toLower() == "true":
    return "@@bool_value@TRUE"
  elif chunk.toLower() == "false:
    return "@@bool_value@FALSE"
  echo "unres " & chunk
  if chunk == "-":
    return "@@oper_sym@MINUS"
  var guess_type, curr_word: string
  var chunk_store = newSeq[string]()
  var has_minus = '-' in chunk
  for i, ch in pairs(chunk):
    if ch == '-':
      if (guess_type == "number" and not curr_word.endsWith("e")) or
        guess_type == "name" or guess_type == "unk":
        chunk_store.add("@@" & guess_type & "@" & curr_word)
        chunk_store.add("@@oper_sym@MINUS")
        curr_word = ""
        guess_type = ""
        continue
    if guess_type == "number":
      if ch notin NUMBER_ADD_CHARS and ch notin NUMBER_FIRST_CHARS:
        if has_minus:
          guess_type = "unk"
        else:
          return "@@unk@" & chunk
    elif guess_type == "name":
      if ch notin NAME_ADD_CHARS and ch notin NAME_FIRST_CHARS:
        if has_minus:
          guess_type = "unk"
        else:
          return "@@unk@" & chunk
    else:
      if ch in NUMBER_FIRST_CHARS or ch == '-':
        guess_type = "number"
      elif ch in NAME_FIRST_CHARS:
        guess_type = "name"
    curr_word = curr_word & ch
  chunk_store.add("@@" & guess_type & "@" & curr_word)
  echo join(chunk_store," ")
  return join(chunk_store," ")

proc tokenate(blob: string): seq[string] =
  var
    kind, lex, map: string
    entries: seq[string]
  var blob = "\\n" & blob
  for cat in RAW_CATS:
    (kind, entries) = process_category(cat)
    for entry in entries:
      (lex, map) = parse_cat_entry(kind, entry)
      if kind in SYM_KINDS:
        if blob.count(lex) > 0:
          blob = blob.replace(lex,pad(map))
      else:
        blob = blob.replace(pad(lex),pad(map))
  let first_tokens = collect(newSeq):
    for chunk in blob.split(' '):
      if len(chunk) > 0:
        if chunk.startsWith("@@"):
          chunk
        else:
          tag_unreserved(chunk)
  let final_tokens = collect(newSeq):
    for chunk in join(first_tokens," ").split(' '):
      if len(chunk) > 0:
        chunk
  return final_tokens

proc main =
  var blob: string
  let lines = readFile(paramStr(1)).strip.split('\n')
  let cleanlines = collect(newSeq):
    for thisline in lines:
      if not thisline.strip.startsWith('#'):
        thisline.strip
  blob = join(cleanlines,"\\n")
  let tokens = tokenate(blob)
  for token in tokens:
    echo token

main()
