# towel-lang

Towel is a programming language intended to optimize the trade-off between learning curve and execution speed. It borrows ideas from Python, bash, Tcl, and Nim.