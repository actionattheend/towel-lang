# A Keyword Glossary of Towel

text here

## Symbols

There are quite a lot of symbols in Towel. Most will be familiar, some will not.

### Separators

#### Line breaks and semicolons

Line breaks are the only semantic whitespace available in Towel. Both Newline and a Carriage Return are treated as a line break, though in linting, Carriage Return is converted to Newline. Line breaks and semicolons terminate a command.

#### Comma

The comma is used sparingly, primarily to separate arguments or elements of an iterable.

### Enclosures

#### Quotation mark types

Standard quotation marks (`"`) delimit strings. There are three other types of decorated quotation marks. `r"` delimits a regular expression, `g"` delimits a glob expression, and `c"` delimits a character. The apostrophe or "single quote" (`'`) is not used to delimit stringlike expressions.

#### Braces

Standard braces (`{`) are used to enclose a nested command, i.e. any command within standard braces is evaluated and substituted by its result. There is a decorated brace, `b{`, which indicates that the commands inside the structure are in the shell scripting language (the b is for bash, but will operate in zsh as well). Multi-line braced blocks are replaced by a value passed to the `return` command, or the value of the variable named `result`. If neither of these occurs, it is replaced by the value of the last expression in the block.

#### Parens

Standard parentheses "`()`" are used to indicate tuples, and nothing else.

#### Brackets

Standard brackets "`[]`" are used to index lists and dictionaries and to indicate slices or ranges, and nothing else.

### Ranges and Slices

#### The .. Operator

Two full-stops in a row (`..`) indicate a range or a slice. Bare mathematical expressions describing integers are the operands, and the resulting range is an array of every integer in the following set:

> `x..y =`

> ` [x, x+1, ..., y-1, y]`

Similarly, a slice of an array can be obtained by indexing an array with a range:

> for an array a = `[0, 1, 2, 3, 4, 5]`:

>  `a[1..4]` = `[1, 2, 3, 4]`

#### Pipe

A pipe (`|`) used on either side of the range operator indicates that the range excludes that extreme. That is:

> `x|..y =`

> ` [x+1, ..., y-1, y]`

and

> `x..|y =`

> ` [x, x+1, ..., y-1]`

and

> `x|..|y =`

> ` [x+1, ..., y-1]`

Slices can be similarly reduced, e.g. `a[1|..4]`, `a[1..|4]`, or `a[1|..|4]`

### Math

#### Arithmetic Operations

The main four standard arithmetic operations function as follows:
* All arithmetic operations use infix notation
* Spaces are optional for arithmetic expressions
* No order of operations convention is upheld. PEMDAS is hot garbage.
* Either parens or braces can be used to indicate order.
* The symbols are the standard programming set: +, -, \*, /.
* When possible, the type of the result will match the type of the operands.
    * A place where this is not possible: division.
    * Therefore, a second type of division, called floor division, exists, symbol //.
    * Floor division returns the greatest integer that will "go into" the dividend, thus `7 // 2 = 3`.

Additional points:
* Modulo is performed with the `mod` keyword, not `%`.
* Exponentiation is performed with `^`. While a positive int to the power of a positive int will return an int, any other scenario will return a float.
* A minus sign preceding a variable name results in the value of that variable with the sign inverted.

#### Comparison

The standard complement of comparators exists: 
* `<`, less than
* `>`, greater than
* `<=`, less than or equal to
* `>=`, greater than or equal to
* `!=` not equal to

Equivalency is tested with `=`, NOT `==`, and CERTAINLY NOT `===`, (seriously, what the hell). "Deeply equal" (meaning same value and same type) is tested with the `is` keyword. No order of comparison is respected, and as in arithmetic, parens or braces can be used to indicate order.

### Variable Operations

#### Assignment

Towel uses `<-` to assign variable values, as in APL and its descendants (notably S and R). This leaves `=` open for equality testing. In addition to `<-`, assignment in the other direction, `->` is also available. This combined with inline evaluation with braces could theoretically make for some hairy situations, so remember: the arrow points at the variable name. The result of an assignment call is a reference, which will be dereferenced only if needed. There is no order of operations in variable assignment.

NB!

```
1 -> a <-3
print a

> 3
```

#### Incrementation

Variable values are incremented with `<-+` or `+->`. In fact, ` x <-+ y` will always be expanded to `x <- x + y`, so any function of `+` is available to you. There is no `++`. There is no `<--`, as that looks weird and I don't like it. Negative numbers are cool.

#### Reference and Dereference

You are not obligated to use `$` to identify your variables. All variables will be treated as references until and unless an instruction acting upon the reference would be illegal, in which case the variable will be dereferenced and the value operated on. However, if you would like to require the compiler to use a variable's reference, use `&` before the variable name to indicate that.

Conversely, if you would like to force the compiler to use a variable's value, and not attempt to perform an operation on the reference, use `$` before the variable name to indicate that.

### Miscellaneous

The percent sign `%` is used to indicate string formatting only (in the standard library).

Comments are made by prepending `#` to a given line. All lines whose first non-whitespace character is `#` will be treated as comments. As in Python, there is no function for multiline comments, prepending `#` is simple in most text editors, and learnable in vi.

Accessing methods and attributes belonging to packages is done with the `:` symbol, e.g. `package:attribute`. Accessing methods and attributes belonging to classes (including classes within a package) is done with `.`, e.g. `class.method()`.

An additional use of the `:` symbol is to pass values to functions. For example, given a function `foo` that takes a `string` called `bar` and a `bool` called `baz`, the call `foo bar:"hoopy" baz:true` will submit those values to the function. The other available method of passing values to functions is to treat each of the parameter names as the headwords of blocks. Therefore, the following is also permitted:

```
foo
  bar
    "hoopy"
  baz
    true
  end
end foo
```

When using `:` notation, the parameters can be in any order. When using block-notation, the parameters must be in the order specified by the function declaration.

## Operator Words

### `in` and `not in`

To determine if an element is in an array, or a substring, character, or pattern is in a string, use `in`: `1 in [1,2,3]` returns `true`. Likewise, use `not in` to determine if the element is absent. The keyword `in` is also used to iterate through every element of an array or dict with `each`.

### Boolean algebra

All commutative boolean operations and inversion are available.

#### `not`

Returns `true` iff the boolean value passed is `false`.

Usage:
```
x <- 3
not (x < 2)

> false
```

#### Other boolean operations

The other boolean operations are: `and`, `or`, `nor`, `nand`, `xor`, and `xnor`. They perform according to this truth table, where the variables are `p` and `q` in that order, e.g. `p and q`:

|p|q|and|or|nor|nand|xor|xnor|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|false|false|false|false|true|true|false|true|
|false|true|false|true|false|true|true|false|
|true|false|false|true|false|true|true|false|
|true|true|true|true|false|false|false|true|

Spelling `true` and `false`: Any combination of lower and upper letters that spell `true` when lowercased are considered the value `"true"`. The same goes for `false`.

### `step`

The `step` operator returns a slice of an array where the members are: element 0, element n, element 2n, ...

Usage:

```
a <- [0,1,2,3,4,5,6,7,8,9,10]
a step 3
```
`> [0,3,6,9]`

### `of`

When declaring an array, use `of` to declare the type of the elements in the array.

### `is`

Use `is` to test that two expressions are the same in value and type.

Usage:
```
x <- 0
y <- 0.0
x is y
```
`> false`

```
0.0 -> x
x is y
```
`> true`

```
a <- "a"
b <- c"a"
(type a is string, type b is string)
```
`> (true, false)`

```
a = b
```
`> true`

```
a is b
```
`> false`

## Block-starting keywords

### Notes on block structure

Any unit of code that is treated as such by the compiler is a block. This means that commands like `function`, `class`, and `block` set off blocks, but within complex blocks there may also be sub-blocks, such as the `named`, `takes`, `returns`, and `does` blocks in a function. Any keyword that indicates to the parser that what follows is or may be a block, is in scope for this section.

In cases like `function`, the presence of certain block-starting keywords performs a double function as a block-ending keyword for the previous block, e.g.:

```
function named foo
  takes
    bar <- string, baz <- float
  returns
    array of string
  does
    ...
  end does
end foo
```

Note that the only blocks that have to be explicitly ended are the `does` block (because there are no more feature blocks of `function` that come after it) and the `function` block.

### `function`

Indicates a function, which can be called elsewhere in the scope. All function blocks are required to have `named` and `does` sub-blocks, and may also have `takes` and `returns` sub-blocks, in the following order: `named`, `takes`, `returns`, `does`.

#### `named`

Indicates the name of the function. This is a special block whose value is expected to be a string. If the value of this block is not a string, you will receive a parser error. Blocks starting with `named` may do two special things:

1. They may start on the same line as the block-starting command (in this case, `function`). This is also the case in any other block that requires or permits a `named` block.
2. They may be a simple unquoted string, as in a variable name.

\* Important note: `named` can also be used to name loops, such as `for` loops, `while` and `until` loops (or their `do` block counterparts) or `block` units.

#### `takes`

Indicates what arguments are expected by the function. The body of a `takes` block is a comma separated list of variable assignments. If the variable assignment is of a type rather than a value (e.g. `a <- int`) the argument is considered required. If the variable assignment is of a value (e.g. `a <- 0`), the argument is considered optional, with that value as the default. If you are concerned about the potential for confusion, you may choose to perform a double-assignment with the type first as follows: `int -> a <- 0`.

Note that the result of a `takes` block is a tuple, but you are not required to use parentheses.

Functions without a `takes` block are not allowed to have values passed to them.

#### `returns`

Indicates the value of the return. The only thing permitted to be the result of a `returns` block is a bare type, e.g. `int`, `string`, `Object`, `MySpecificObject`, `array of char`, or `(char, int, float)`. The value of this block can be listed on the same line as `returns`, e.g. `returns float`.

Functions without a `returns` block return `true` by default.

#### `does`

The command block of the function. If the special variable `result` has a value at the end of this block, it will be considered the value of this block, unless `return` has been reached elsewhere in the block's execution. If neither of these conditions are met, the value returned by the last line of the block will be returned.

A `does` block is usually terminated by `end does`, but any standard block terminator will work.

### `class`

Indicates that the following block is a class describing an object. Class blocks must take at least one of the following: a `constructor` block and any number of `function` blocks. They may also take a `destructor` block.

#### `constructor`

Constructor blocks take an optional `takes` block and a required `does` block. Variables assigned or declared within the constructor `does` block become and remain class variables, without a reference to constructs such as `this` or `self`.

#### `destructor`

Destructor blocks take an optional `returns` block and a required `does` block. The `result` variable may be used here, as can the rest of the function return rules.

#### Class functions

Class function blocks operate like normal blocks except that the special variable `private` can be set in the `takes` block. The standard default is `false`. If `private` is set to `true`, the function will be inaccessible via dot-navigation, and will only be callable by other functions within the class. All class functions have access to the variables declared in the constructor. If the class function is intended to write to class variables, be sure these are declared or assigned in the constructor.

### `generator`

Indicates that the following block is a generator. Generators, as in Python, are functions that yield multiple values iteratively rather than perform a function to completion then return a single value. Blocks beginning with `generator` require a `named` block, a `returns` block, and a `does` block, and can take a `takes` block, in this order: `named`, `takes`, `returns`, and `does`. Unlike Python generators, Towel generators use `return` to return the value and rely upon different declarative syntax to indicate that they are generators.

### `context`

The following are synonyms that can be used for `class` blocks when it makes more sense to think of a class as a context manager:

- `context` for `class`
- `enter` for `constructor`
- `exit` for `destructor`

### `with`

A `with` block allows for variables to be assigned only for the duration of the block. When the block ends, all of the variables are de-assigned. If `new Object` is called during the assignment block, its `destructor` will be called when the block ends. All `with` blocks contain an obligatory `do` sub-block. Between `with` and `do` is the assignment block, between `do` and `done` is the execution block, as follows:

```
with foo <- new Object "bar" 1
do print foo.goo
```

or

```
with
  foo <- 0
  bar <- new Object "baz" 0
do
  print bar.goo
  print {1 +-> foo}
done
```

### `while` and `until`

A `while` block iterates its `do` sub-block as long as the condition indicated evaluates to `true`. The value of the final line of the `while` sub-block, cast to a boolean, is considered the condition. An `until` block does the same, except it iterates as long as the condition is `false`. The `do` block may precede `while` or `until`. Here are the possible configurations:

```
while
  condition
do
  execution
done
```

```
until
  condition
do
  execution
done
```

```
do
  execution
while
  condition
end while
```

```
do
  execution
until
  condition
end until
```

### `for`

A for block iterates over a range. That range can be expressed as any iterable expansion, performed with `each`, e.g. `each x in 4..20`, `each foo in bar`, `each (key,value) in goo`.The maximum value of a numerical range can be used alone, which will be expanded to 0..|n, i.e. `each c in 50` is the same as `each c in 0..|50`. Also, if a variable name is not used, the `type iterator varname` value (default `i`) is used. So, `each in 50` = `each i in 0..|50`. Because this no longer implies a useful purpose for `each in`, it can be elided here too: `for 3` is the same as `for each i in 0..|3`, and `for foo` is the same as `for each i in foo`.

Every `for` block requires a `do` sub-block.

An example:

```
for 10
do
  print 10 - i
done
print "Blast off"
```

### `if`, `then`, and `else`

An `if` block is fairly intuitive, here's a template:

```
if
  condition
then
  execution
else
  execution
fi
```

As is often the case, the ending keyword could be exchanged for other ending keywords, `done`, `end`, and the like. Instead of `elif`, use `else if` without nesting.

```
if
  condition
then
  execution
else if
  condition
then
  execution
else
  execution
fi
```

As often is the case, blocks can be one-liners. In this special case, `if`, the condition, and `then` can be placed on the same line:

```
if foo > bar then
  print "foo is greater"
else if foo < bar then
  print "bar is greater"
else
  print "there is peace between foo and bar"
fi
```

### `map`

The `map` command takes an iterable type, performs a function on each of its elements, and returns an iterable of the same type. With `map`, `filter`, and the `fold` commands, use `by` to open the function block. By convention, use `k` and `v` for the variables in your function if mapping a dictionary, otherwise use `x`. If you have named variables those things in your code, you should consider renaming them rather than choosing new names for your closure??? variables.

```
a <- [1,2,3]
map a by
  x ^ 2
end map

> [1, 4, 9]
```

Again, blocks can be single-line, which obviates the need for a block-ending command. Thus, `map a by x ^ 2`.

### `filter`

The `filter` command takes an iterable type, and returns an iterable of that same type containing all the elements that match the condition in the following block. The `by` block in the map command was for a function, but for filter, it is a condition block.

```
foo <- ["bar", "baz", "goo"]
filter foo by
  x[0] = "b"
end filter

> ["bar", "baz"]
```

Single-line: `filter foo by x[0] = "b"`

### `fold left` and `fold right`

These commands reduce iterable types by taking elements of the iterable and performing a function on them with regard to a running accumulator (always called `result`). The return type and value of this command is always the type and value of `result`. Currently, you can't split `fold` from its direction.

```
a <- [1,1,2,3,5]
fold left a by
  result <- result + {string x}
end fold

> "53211"
```

### `eval`

Executes code in the shell that called the program. Mostly interchangeable with `b{}`.

### `try` and `except`

Attempts to execute the command block. If it fails, and an `except` block immediately follows, will execute the commands in `except`. If it succeeds, the `except` commands will not be executed. There is no `finally`—that's just called "code that comes after your except block". Exceptions are strings, which are passed to the except block as the special variable `exception`.

```
try
  z <- 3 + "f"
except
  print exception
end try

> "ERROR, Operation not allowed for these two types: Addition for int and string."
```

### `async` and `await`

Create and listen for asynchronous coroutines. The `async` command only takes one argument: the declaration of a named evaluation block (usually either `function` or `block`). The `await` command returns the value of the evaluated `async` block. The `await` command cannot be called on any other function.

Please note that calling an `async` function is the same as scheduling it. There is no `run` or `schedule` command. Towel assumes you remember which functions are async and which are not. Attempting to `await` a non-async function will error.

More asynchronous support can be found in the `coroutines` package.

```
example TK
```

### `set type`

The `set type` keyword is the basic command for meta-programming and type extension in Towel. Without calling `use` on the `meta` module, there are only a few options available for users of `set type`:

* Changing conversion values of types.
* Declaring default values of user-defined types.
* Declaring conversion values of types that don't normally convert to each other.
* Set special varnames with `varname`, as in `i` for `for` loops where a variable is not declared, `exception` for error results of `try` blocks.
* Set synonyms with `synonym`.

Unlike most other commands, `set type` requires an single-line, inline block to follow the keyword. Specifically, `set type <inline type characterization> <value>` is the template for these commands, where `<inline type characterization>` is a required block that also serves as the start of the `<value>` block, which is also required, though it can be inline or not.

```
a <- false
b <- true
print "before: ${int a}, ${int b}"

set type {bool -> int}
  false -> -1
  true -> 0
end set type

print "after: ${int a}, ${int b}"

> before: 0, 1
> after: -1, 0
```

```
a <- false
b <- true

try print "before: ${string a}, ${string b}"
except print exception

set type {bool -> string}
  false -> "wrong"
  true -> "correct"
end set type

print "after: ${string a}, ${string b}"

> ERROR, Direct conversion not allowed for these two types: bool to string.
> after: wrong, correct
```

```
for 3
  print i
end

set type {for varname} "n"

for 3
  print i
end

> 1
> 2
> 3
ERROR, Variable "i" is not declared. For `for` loops, the default varname is "n".
```

## Stop keywords

There isn't much to say here: there are four keywords that can be used to end blocks. They are all synonyms for each other, but idiomatically some make more sense than others in various situations. Any word can be used after any stop keyword, and unless this word is the name of a `block` or an iterating block, it will have no effect. If the this word *is* the name of a `block` or an iterating block, it will end that unit. Note that in a nested-iteration situation, using a stop-word to end the outer loop without ending the inner loop will result in an error—if you need to break out of an outer loop, use `break` and the name of the loop.

```
for 50 named outer do
  for j in 2 named inner do
    if j = 1 then end outer
  done
done

ERROR, Keyword in incorrect location. Stop-word `end` is not a command, consider using `break`.
```

### `end`

This is the very basic stop word, useful in any given situation.

### `stop`

A complete synonym for `end`.

### `done`

Only use this to end blocks that begin with `do`.

### `fi`

Only use this to end the group of blocks associated with an `if` statement.

## Command Keywords

The following are the more run-of-the-mill commands.

### `use` and `import`

The `use` keyword allows the program access to a library (which can add and modify keywords). The `import` keyword allows the program access to a module (a collection of additional classes).

Usage:
```
use math
import time
```

To import one or more classes from a module without the others, use:

```
import time.Timer, time.TimeObject
```

### `reset type`

Resets a `<type characterization>` after having been set by `set type`.

### `ref` and `deref`

The `ref` keyword is equivalent to `&` before a variable name, and the `deref` keyword is equivalent to `$` before variable name.

### `promote` and `relegate`

The `promote` keyword is similar to `upvar` in Tcl—it allows a variable to be accessed by the scope "up" from it, for example:

```
block named foo
  g <- 100
  promote g
end foo

print g

> 100
```

Normally attempting to access `g` outside of the block in which it was assigned would cause an error. You can `relegate` a variable back out of scope so that you don't mess it up further. Performing `relegate` on a variable inside a `function main` block or declared outside of all functions removes it from all namespaces.

```
block named foo
  block named bar
    g <- 100
    promote g
  end bar

  print g
  relegate g
  print g
end foo

> 100
ERROR, No variable named `g` in this scope. There is a variable named `g` in the parent scope.
```

### `print`

Writes to the standard output channel (stdout). Normally appends a newline. This can be modified by setting `endofline` to something else, including "".

```
for each letter in [c"a"..c"z"] do
  print endofline:"" letter
end for

> abcdefghijklmnopqrstuvwxyz
```

### `error`

Halts execution and writes to the standard error channel (stderr). To write to stderr without halting, modify `halt` to `false`. The `endofline` attribute is also available.

```
palindrome <- "satan oscillate my metallic sonatas"

reversed <- string

print "Metallic sonata oscillation progress" endofline:""
for each c in palindrome do
  error "." halt:false endofline:""
end for
```

### `find` and `search`

Use `find` to obtain the index of the first element in an `iterable` that matches the object passed to `find`. Use `search` to obtain an array of the indices of all elements in an `iterable` that contain the object passed to `search`. Both commands can accept `r"` regular-expression strings or `g"` glob strings. Obtain an array of indices with `find` by setting `all` to `true`. Obtain only the first index with `search` by setting `all` to `false`.

The syntax is `<verb> <element> <array>` and as is often the case, you can use `in` to remember the order, which the parser will discard without harming the program, e.g. `find x in y` or `search g"*ello*" in greetings`

### `length`

Return the `int` length of the object passed.

### `continue` and `break`

Use `continue` to skip the rest of the code within a loop iteration and continue on the next iteration. Use `break` to skip the rest of the code within a loop iteration and stop iterating, leaving the loop. Both commands may take the name of a loop to continue or break from, even if it is not the immediate parent loop of the code being executed.

```
# assume a function isPrime that returns a boolean representing whether an int is prime
```

Session 1

```
for level in 0..2 named outer do
  for 1..10 named inner do
    n <- i + (10 * level)
    if n = 1 do continue
    if isPrime n do 
      print "{n} is prime"
      if n > 10 do break
    fi
  end for inner
end for outer

> 2
> 3
> 5
> 7
> 11
> 23
```

Session 2

```
for level in 0..2 named outer do
  for 1..10 named inner do
    n <- i + (10 * level)
    if n = 1 do continue
    if isPrime n do 
      print "{n} is prime"
      if n > 10 do break outer
    fi
  end for inner
end for outer

> 2
> 3
> 5
> 7
> 11
```

### `ignore`

Ignores a value. As in Nim, values cannot be "left hanging". Every value returned in a command must be dealt with in some way. One of those ways is to ignore it.

### `push` and `pop`

Add an element to an `iterable` or obtain the last element in an `iterable` while removing it. Use `to` and `from` to remember the order of arguments or enhance readability, like `in` in `find`/`search` commands, they will be discarded.

```
[8] -> x
push 6 to x
print x
push 9 to x
print x
ignore pop from x
push 7 to x
for each n in "5309" do
  push {int n} to x
end for tutone
print x

> [8, 6]
> [8, 6, 9]
> [8, 6, 7, 5, 3, 0, 9]
```

Use `each` with `push` to add multiple elements to a single iterable individually. Use `add` as a synonym for `push`.

### `skip`

Pass over the next instance of a specifically named block, e.g. `skip foo` passes over the next block named `foo`. Use `all:true` to skip every block named `foo`.

```
for do
  name <- "Joe"
  userinput <- {ask "What's your name? "}

  if name = "Joe" do skip rename

  block rename
    print "renaming"
    name <- userinput
  end rename

  print "Hey {name}, whaddaya know?"
end for

) What's your name? Joe
> Hey Joe, whaddaya know?
) What's your name? Mary
> renaming
> Hey Mary, whaddaya know?
...
```

### `parse`

Bare-type implicit parsing is permitted in Towel. Use `parse <value> as type` where it makes more sense or is more readable, or where `type` is a string description of a type.

### `return`

From inside a function or non-iterating block, return the given value and end execution of the block, returning to the function call in the case of a function, or proceeding to the end of the block in the case of a block.

### `the following`

This is a special variable that indicates to the parser that whatever comes next should replace it in any command, e.g.

```
[0] -> x
push the following to x
block item to push
  n <- 7
  n <- n*2
  n <- n-4
  n <- n/2
end item to push

print x

> [0, 5]
```

Use `each of` in place of `each` where it makes sense.

### `next`

Having instantiated a generator, get the next value generated by it.

### `delete`

Delete an element from an iterator, in the form `itername[index]` where `itername` is the variable name of the iterator and `index` is either the numerical index in the case of an array or string, or the key in the case of a dictionary.

### `tally`

Like Python's `enumerate`, Go's `range`, and Nim's `pairs`, yield a series of tuples of (`index`, `value`) from an iterator. Use `items` as a synonym for dictionaries if you like.

`for each i, val in {tally j}`

### `type`

Return a string describing the type of a value. Does not return a "type object".

### Arithmetic commands

* The `mod` command returns the modulo of two `int`s, in the form `8 mod 5`.
* The `abs` command returns the absolute value of an `int` or `float`.
* The `root` command returns the `float` value of the nth root of an `int` or `float`, in the form `3 root 27`. If used without an operand, it will return the square root.
* The `round` command returns the `int` value nearest a `float`, where e.g. 1.5 is rounded to 2.
* The `floor` command returns the `int` value nearest to, but less than or equal to, a `float`.
* The `ceiling` command returns the `int` value nearest to, but greater than or equal to, a `float`.

### Array calculation commands

* The `sum` command returns the additive value of all of the elements of an iterable. The `sum` of an array of strings is the concatenation of all the strings.
* The `average` command returns the arithmetic mean `float` value of all of the elements in an array of `float` or `int` values.
* The `max` command returns the highest value of all of the elements of an iterable of a type against which comparison is meaningful.
* The `min` command returns the lowest value of all of the elements of an iterable of a type against which comparison is meaningful.

### Other array commands

* Use `sort` to perform an in-place sort of an iterable by element value, ascending, and return a reference to the sorted iterable object. Reverse order by setting `reverse` to `true`. As the default function that determines sort order is set to the `by` attribute, change this attribute to change the function that determines sort order (there is no way to change the sorting algorithm, just to change what values are considered "greater than" others). `by: the following` is permitted.
* Use `dedupe` to perform in-place deduplication of an iterable and return its reference. The default function that determines whether elements match is set to the `by` attribute; changing this will change what elements are considered duplicate.

### `new`

Instantiates an object. Pass arguments with `name:value` or in the order outlined in the object's class `constructor`.

### `ask`

Return a string from `stdin`. Set `chop` to `true` to eliminate trailing whitespace.

### String / array commands

*NB: String and array commands are not in-place. They return new objects, leaving the originals intact.*

* Use `split` to divide a string into an array of substrings, as divided by a provided divider. The divider is not returned. Use `""` as the divider to split the string into single-character strings. Set `discardEmpty` to `true` to skip any elements that would be blank. Use `by` to keep the arguments in order.
* Use `join` to create a string from an array of `string`s or `char`s, adding a divider between each two elements. Use `""` as the divider to create a continuous string. Use `with` to keep the arguments in order.
* Use `trim` to remove whitespace from both sides of a string. Set `before` to `false` to trim only trailing whitespace; set `after` to `false` to trim only leading whitespace. Set `target` to a string, `r"` or `g"` pattern or array of strings to remove these characters instead of whitespace. Every matching substring will be removed from the end of the string, and removals will be repeated until no more characters can be removed.

### Dictionary commands

* Use `keys` to return an array of keys.
* Use `values` to return an array of values.
* Use `items` to return an array of tuples of the form `(key, value)`.

## Types

Fundamentally, there are only five types: two numeric types, two string types, and a boolean type. Beyond that, there are three types of sets: arrays, dictionaries, and tuples. Arrays must be all the same type, but can be expanded. Dictionaries (aka hash maps, objects) can use any of the single-value types as keys and any available type as values, but all keys must be of the same type, and all values must be of the same type. Further, there are three meta-types: numbers, text types, and iterables.

### `int`

A whole number, short for "integer". Fundamentally, this is all you need to know in Towel. You should not experience underflow or overflow, and most of the time these shouldn't take up much more than they need in memory.

### `float`

A rational number, terminating number. Implicit conversion from `float` to `int` is the same as calling `floor` on the `float`.

### `string`

A list of text characters, encoded as UTF-8. Additional encodings are available in the `encoding` package. Strings are also treated as arrays of one-character strings.

### `char`

Short for "character", often pronounced like the word for burnt wood. The `char` type is a bit of a cheat. It is a one-character string that also has a quasi-numerical value, in that it can be used in ranges and comparisons and the like.

### `bool`

Short for Boolean, after George Boole, a mathematician who invented an algebra of binary/truth values. Either `true` or `false`.

### `array`

A list of items of the same type, indexed by `int`s in order, starting with 0. (This can be modified with `set type`.) Arrays do not have to have a length fixed at declaration.

### `dict`

Short for "dictionary". A list of pairs of values. One is the key: an index of any type, though all indices/keys in a single `dict` must be the same type. The other is the value. All values in a single dict must be the same type.

### `tuple`

Pronounced "TUH-pl" apparently. As in "quintuple" etc. A list of values, which can be multiple types. Tuple length is invariant.

### `nothing`

The dreaded null type. This is the default type of things that don't otherwise have a type value. You can set variables of any type to be equal to `nothing`, but there are probably better solutions to whatever you're up to.

### `number`

An `int` or `float` (or any other type as extended by metaprogramming that has been set as a `number`.)

### `text`

A `string` or `char` (see above for extension language).

### `iterable`

An `array`, `dict`, `tuple`, `string`, range, slice, or object returned by a generator.